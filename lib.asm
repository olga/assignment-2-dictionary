
section .data
 %define END_OF_STRING 0x0
 %define SYS_WRITE 0x1
 %define STDERR 0x2
 %define STDOUT 0x1
 %define ONE_SYMBOL 0x1
 %define NULL 0x0
 %define DECIMAL_BASE 0xA
 %define NULL_ASCII_NUM_CODE 0x30
 %define END_ASCII_NUM_CODE 0x39
 %define MINUS_SYMBOL '-'
 %define PLUS_SYMBOL '+'
 %define NEW_LINE_SYMBOL 0xA
 %define TAB_SYMBOL 0x9
 %define SPACE_SYMBOL 0x20
 %define RETURN_CODE 60

section .text
global exit
global string_length
global print_string
global print_error  ;new one
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global read_string  ;new one
global parse_uint
global parse_int
global string_copy
 
NEW_LINE: db NEW_LINE_SYMBOL
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, RETURN_CODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .count_loop:
        cmp byte[rdi+rax], END_OF_STRING
        je .end_length
        inc rax
        jmp .count_loop
    .end_length:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    push rsi
    call string_length
    pop rsi
    mov rdi, STDOUT
    mov rdx, rax	; size of message
    mov rax, SYS_WRITE
    syscall
    .end_print:
    ret

print_error:
	mov rsi, rdi
    push rsi
    call string_length
    pop rsi
    mov rdi, STDERR
    mov rdx, rax	; size of message
    mov rax, SYS_WRITE
    syscall
    .end_print:
	mov rax, 1
    ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
    mov rsi, rsp
    mov rdi, STDOUT
    mov rdx, ONE_SYMBOL
    mov rax, SYS_WRITE
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rsp	;save regs
    push rbx
    push r12
    
    xor r12, r12
    xor rdx, rdx
    mov rax, rdi
    mov rbx, DECIMAL_BASE
    
    dec r12
    dec rsp
    mov byte[rsp], END_OF_STRING
    
    .to_decimal_ascii:
        xor rdx, rdx
        div rbx
        test rax, rax
        jz .printing
        add rdx, NULL_ASCII_NUM_CODE
        dec r12
        dec rsp
        mov byte[rsp], dl
        jmp .to_decimal_ascii
    
    .printing:
        add rdx, NULL_ASCII_NUM_CODE    ;convert last one to ascii code
        dec r12
        dec rsp
        mov byte[rsp], dl
        mov rdi, rsp
        call print_string
        sub rsp, r12

    .end:
    pop r12
    pop rbx
    pop rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, NULL
    jge .print

    .if_minus:
        push rdi
        mov rdi, MINUS_SYMBOL
        call print_char
        pop rdi
        neg rdi

    .print:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .checking:
        mov ah, byte[rdi]
        cmp ah, byte[rsi]
        jne .false
        test ah, ah
        je .true
        inc rdi
        inc rsi
        jmp .checking
    .true:
        mov rax,1
        ret
    .false:
        mov rax,0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi

    sub rsp, 0x8
    mov rsi, rsp
    mov rdx, 1  ; size of 1 symbol
    syscall                                         

    cmp rax, -1
    je .error
    test rax, rax
    jz .error
    
    mov al, byte[rsp] 
    jmp .end
    
    .error:
        xor rax, rax
    
    .end:
        add rsp, 8
        ret 

read_char_255:
    

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax

    push r12
    push r13
    push rbx

    mov rbx, rdi
    mov r12, rsi
    xor r13, r13
    
    .reading:
        call read_char

        cmp al, NEW_LINE_SYMBOL
        je .skip_space_symbols
        cmp al, TAB_SYMBOL
        je .skip_space_symbols
        cmp al, SPACE_SYMBOL
        je .skip_space_symbols

        cmp r13, r12
        jnl .fault
        test rax, rax
        jz .success
        mov byte[rbx + r13], al
        inc r13
        jmp .reading

    .skip_space_symbols:
        test r13, r13
        jz .reading
        jnz .success

    .fault:
        xor rax, rax
        jmp .end

    .success:
        mov byte[rbx + r13], END_OF_STRING
        mov rax, rbx
        mov rdx, r13
        jmp .end

    .end:
        pop rbx
        pop r13
        pop r12
        ret

read_string:
    xor rax, rax

    push r12
    push r13
    push rbx

    mov rbx, rdi
    mov r12, rsi
    xor r13, r13

    .reading:
        call read_char
        cmp al, NEW_LINE_SYMBOL
        je .skip_space_symbols
        cmp r13, r12
        jnl .fault
        test rax, rax
        jz .success
        mov byte[rbx + r13], al
        inc r13
        jmp .reading

    .skip_space_symbols:
        test r13, r13
        jz .reading
        jnz .success

    .fault:
        xor rax, rax
        jmp .end

    .success:
        mov byte[rbx + r13], END_OF_STRING
        mov rax, rbx
        mov rdx, r13
        jmp .end

    .end:
        pop rbx
        pop r13
        pop r12
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rax, 0x0
    push r12
    push r13

    cmp byte[rdi], END_OF_STRING
    je .end

    .first_time:
        cmp byte[rdi], NULL_ASCII_NUM_CODE
        jl .end
        cmp byte[rdi], END_ASCII_NUM_CODE
        jg .end

    .pre_parsing:
        xor r12, r12
        xor r13, r13

    .parsing:
        mov al, byte[rdi]
        sub al, NULL_ASCII_NUM_CODE
        add rax, r12
        inc r13
        inc rdi
        cmp byte[rdi], NULL_ASCII_NUM_CODE
        jl .end
        cmp byte[rdi], END_ASCII_NUM_CODE
        jg .end
        mov r12, DECIMAL_BASE
        mul r12
        mov r12, rax
        xor rax, rax
        jmp .parsing

    .end:
    mov rdx, r13
    pop r13
    pop r12
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx

    .if_plus:
        cmp byte[rdi], PLUS_SYMBOL
        jne .end_if_plus
    .if_plus_true:
        inc rdi
        call parse_uint
        inc rdx
        jmp .end
    .end_if_plus:

    .if_minus:
        cmp byte[rdi], MINUS_SYMBOL
        jne .end_if_minus
    .if_minus_true:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        jmp .end
    .end_if_minus:


    .if_without_sign:
        jmp parse_uint

    .end:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push r12
    push rdi
    push rsi
        call string_length
    pop rsi
    pop rdi
    inc rax ; haven't counted terminator
    mov r12, rax
    cmp rdx, rax
    jl .error

    .coping:
        mov al, byte[rdi]
        mov byte[rsi], al
        cmp byte[rdi], END_OF_STRING
        je .end
        inc rsi
        inc rdi
        xor rax, rax
        jmp .coping

    .error:
        xor r12, r12

    .end:
    mov rax, r12
    pop r12
    ret