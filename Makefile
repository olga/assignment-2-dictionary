P=$(shell pwd)

.PHONY: make build clean rebuild

./%.o: $(P)/%.asm
	nasm -f elf64 -g -o $@ $<

build: ./main.o ./lib.o ./lib.inc ./dict.o ./dict.inc ./words.inc ./colon.inc
	ld -o ./executable ./lib.o ./dict.o ./main.o

test: ./test.o ./lib.o ./lib.inc ./dict.o ./dict.inc ./words.inc ./colon.inc
	ld -o ./executable ./lib.o ./dict.o ./test.o

clean:
	rm -f ./*.o

rebuild: clean build