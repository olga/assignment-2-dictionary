%include "lib.inc"
%include "colon.inc"
%include "dict.inc"
%include "words.inc"

section .bss

%define MAX_SYMBOLS 255

buff: resb 256

section .rodata

READING_ERROR: db "there is a reading error", 0
FIND_WORD_ERROR: db "word haven't found", 0

WRITE_KEY: db "write key line:", 0

section .text

error:
	call print_error
	jmp exit

global _start

_start:
	mov rdi, WRITE_KEY
	call print_string
	call print_newline
	mov rdi, buff
	mov rsi, MAX_SYMBOLS
	call read_string
	test rax, rax
	jz .read_err

	mov rsi, list_head
	mov rdi, buff
	call find_word
	test rax, rax
	jz .find_err

	mov rdi, rax
	call print_word
	call exit


	.read_err:
	mov rdi, READING_ERROR
	jmp error

	.find_err:
	mov rdi, FIND_WORD_ERROR
	jmp error

call exit