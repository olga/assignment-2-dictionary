;unit-tests!!
%include "lib.inc"
%include "colon.inc"
%include "dict.inc"

section .data

 %define END_OF_STRING 0x0
 %define ERROR_POINT 0x1
 %define SKIP_LABEL_BYTES 0x8
 %define STDOUT 0x1

colon "lastkey", lastlabel
db "lasttext", END_OF_STRING

colon "centralkey", centrallabel
db "centraltext", END_OF_STRING

colon "beginkey", beginlabel
db "begintext", END_OF_STRING

colon "testkey1", testlabel1
db "testtext1", END_OF_STRING

colon "", testlabel2
db "testtext2", END_OF_STRING

colon "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia volupta", testlabel3
db "testtext3", END_OF_STRING

colon "!@#$%^&*()", testlabel4
db "testtext4", END_OF_STRING

colon "6", testlabel6
db "6", END_OF_STRING


testkey1: db "testkey1", END_OF_STRING
testkey2: db "", END_OF_STRING
testkey3: db "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia volupta", END_OF_STRING
testkey4: db "!@#$%^&*()", END_OF_STRING
testkey5: db "testkey5", END_OF_STRING
testkey6: db "6", END_OF_STRING

;-----errors-messages-----

colon_error: db "macro colon is working incorrectly", END_OF_STRING
find_word_error: db "find_word has error with ", END_OF_STRING

;-------------------------

correct_message: db "correct", END_OF_STRING

section .text


testdata:
	mov rax, [beginlabel]
	mov rax, [rax]
	mov rax, [rax]
	mov rdi, colon_error
	test rax, rax
	jnz print_error
	xor rax, rax
	ret

test_find_word:
	push rdi
	call find_word
	add rax, SKIP_LABEL_BYTES
	mov rsi, rax
	call string_equals
	test rax, rax
	jnz .correct
	mov rdi, find_word_error
	call print_error
	pop rdi
	call print_error
	mov rax, ERROR_POINT
	ret
	.correct:
	pop rdi
	xor rax, rax
	ret

test_not_find_word:
	push rdi
	call find_word
	test rax, rax
	jz .correct
	mov rdi, find_word_error
	call print_error
	pop rdi
	call print_error
	mov rax, ERROR_POINT
	ret
	.correct:
	pop rdi
	xor rax, rax
	ret

global _start


_start:
	xor rbx, rbx
	call testdata
	add rbx, rax
	mov rdi, testkey1
	mov rsi, list_head
	call test_find_word
	add rbx, rax
	mov rdi, testkey2
	mov rsi, list_head
	call test_find_word
	add rbx, rax
	mov rdi, testkey3
	mov rsi, list_head
	call test_find_word
	add rbx, rax
	mov rdi, testkey4
	mov rsi, list_head
	call test_find_word
	add rbx, rax
	mov rdi, testkey5
	mov rsi, list_head
	call test_not_find_word
	add rbx, rax
	test rbx, rbx
	jne .end
	mov rdi, correct_message
	call print_string
	.end:
	call exit