%define list_head 0

%macro colon 2
	%2:	dq list_head
		db %1, 0	;key

	%define list_head %2
%endmacro