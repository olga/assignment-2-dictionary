%include "lib.inc"

global find_word
global print_word


 %define END_OF_STRING 0
 %define SKIP_LABEL_BYTES 0x8

;rdi - key that we are sicking
;rsi - start of dict

find_word:
	add rsi, SKIP_LABEL_BYTES

	push rsi
	push rdi
	call string_equals
	pop rdi
	pop rsi

	sub rsi, SKIP_LABEL_BYTES
	test rax, rax
	jnz .found
	cmp byte[rsi], END_OF_STRING
	jz .not_found
	mov rsi, qword[rsi]
	jmp find_word
	
	.found:
		mov rax, rsi
		ret

	.not_found:
		xor rax, rax
		ret


print_word:
	add rdi, SKIP_LABEL_BYTES
	.skip_key:
		inc rdi
		call string_length
		add rdi, rax
	inc rdi
	jmp print_string